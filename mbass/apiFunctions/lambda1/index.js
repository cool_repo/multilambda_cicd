let envConfig=require("./envConfig.json")
let config = require("./configs/WebConfig" + envConfig.environment + ".json")
exports.handler = function(event, context, callback) {
    callback(null,{
        statusCode:200,
        body:JSON.stringify({
            message:"this is lambda1 v1 with env : " +config.env
        })
    })
}